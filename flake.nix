{
  description = "Description for the project";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";
    devshell.url = "github:numtide/devshell";
  };

  outputs = inputs @ {
    nixpkgs,
    flake-parts,
    devshell,
    ...
  }:
    flake-parts.lib.mkFlake {inherit inputs;} {
      imports = [
        devshell.flakeModule
      ];
      systems = ["x86_64-linux" "aarch64-linux" "aarch64-darwin" "x86_64-darwin"];
      perSystem = {
        config,
        self',
        inputs',
        pkgs,
        system,
        ...
      }: let
        p2nixOverlay = _: prev: let
          poetry2nix = prev.poetry2nix.overrideScope' (_: super: {
            # Downgrade setuptools-scm in the default overrides to a version that doesn't depend on importlib-metadata for Python 3.7
            # We'll use this poetry2nix to build poetry itself as well
            defaultPoetryOverrides = super.defaultPoetryOverrides.extend (_: pysuper: let
              inherit (pysuper) setuptools-scm;
              setuptools-scm_6 = pysuper.setuptools-scm.overridePythonAttrs (_: {
                pname = "setuptools_scm";
                version = "6.4.2";
                src = pysuper.fetchPypi {
                  pname = "setuptools_scm";
                  version = "6.4.2";
                  sha256 = "sha256-aDOsZcbtlxGk1dImb4Akz6B8UzoOVfTBL27/KApanjA=";
                };
              });
            in {
              setuptools-scm =
                if prev.lib.versionOlder pysuper.python.version "3.8"
                then setuptools-scm_6
                else setuptools-scm;
            });
          });
        in {
          inherit poetry2nix;
          poetry = prev.poetry.override {
            # We override the poetry2nix instance used to build poetry itself
            inherit poetry2nix;
          };
        };

        overrides = self: super: {
          pyspark = super.pyspark.overridePythonAttrs (old: {
            nativeBuildInputs = (old.nativeBuildInputs or []) ++ [self.pypandoc];
            # pyspark doesn't constrain pypandoc's version. The `convert` syntax was deprecated and removed in 1.8
            patchPhase =
              (old.pathPhase or "")
              + ''
                substituteInPlace setup.py --replace \
                  "pypandoc.convert" \
                  "pypandoc.convert_file"
              '';
          });
          duckdb = super.duckdb.overridePythonAttrs (old: {
            buildInputs = (old.buildInputs or []) ++ [self.setuptools-scm];
          });
        };
        dev = pkgs.poetry2nix.mkPoetryEnv {
          projectDir = ./.;
          python = pkgs.python37;
          overrides = pkgs.poetry2nix.overrides.withDefaults overrides;
        };
      in {
        _module.args.pkgs = import nixpkgs {
          inherit system;
          overlays = [p2nixOverlay];
        };
        devshells.default = {
          packages = [pkgs.python37 pkgs.poetry];
        };
        devShells.dev = dev.env;
        packages.dev = dev;
      };
    };
}
